﻿iRowCount = Datatable.getSheet("TC04_TESTWEB [TC04_TESTWEB]").getRowCount
username = Trim((DataTable("Username","TC04_TESTWEB [TC04_TESTWEB]")))
password = Trim((DataTable("Password","TC04_TESTWEB [TC04_TESTWEB]")))
note = Trim((DataTable("Note","TC04_TESTWEB [TC04_TESTWEB]")))

Call FW_TransactionStart("TC04 TESTWEB - Open web DEMOSTORE.x-CART")
Call FW_OpenWebBrowser("https://demostore.x-cart.com/","CHROME")
Call FW_TransactionEnd("TC04 TESTWEB - Open web DEMOSTORE.x-CART")

Call FW_TransactionStart("TC04 TESTWEB - Click button for login")
Call FW_WebElement("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Sign in / sign up")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("Sign in / sign up").Click @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionEnd("TC04 TESTWEB - Click button for login")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("Sign in / sign up").Click

Call FW_TransactionStart("TC04 TESTWEB - Input Username") @@ script infofile_;_ZIP::ssf2.xml_;_
Call FW_WebEdit("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","login", username)
Call FW_TransactionEnd("TC04 TESTWEB - Input Username")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebEdit("login").Set "gggg" @@ script infofile_;_ZIP::ssf3.xml_;_


Call FW_TransactionStart("TC04 TESTWEB - Input Password") @@ script infofile_;_ZIP::ssf2.xml_;_
Call FW_WebEdit("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","password", password)
Call FW_TransactionEnd("TC04 TESTWEB - Input Password")


Call FW_TransactionStart("TC04 TESTWEB -  Click Sign in")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Sign in")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("Sign in").Click @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionEnd("TC04 TESTWEB - Click Sign in")


Call FW_TransactionStart("TC04 TESTWEB - Select Manu Hot deals")
Call FW_WebElement("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Hot deals") @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionEnd("TC04 TESTWEB - Select Manu Hot deals")


Call FW_TransactionStart("TC04 TESTWEB - Select Manu Bestsellers")
Call FW_Link("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Bestsellers")
Call FW_TransactionEnd("TC04 TESTWEB - Select Manu Bestsellers")

'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("Fashion").Click
Call FW_TransactionStart("TC04 TESTWEB - Select Manu Fashion")
Call FW_Link("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Fashion")
Call FW_TransactionEnd("TC04 TESTWEB - Select Manu Fashion")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Fashion").Click


Call FW_TransactionStart("TC04 TESTWEB - Select Shoes Category")
Call FW_WebElement("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Shoes")
Call FW_TransactionEnd("TC04 TESTWEB - Select Shoes Category")

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company").WebElement("Shoes").Click


Call FW_TransactionStart("TC04 TESTWEB - Select Products White Lace-Up Mesh Trainers")
Call FW_Image("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","White Lace-Up Mesh Trainers")
Call FW_TransactionEnd("TC04 TESTWEB - Select Products White Lace-Up Mesh Trainers")
 @@ script infofile_;_ZIP::ssf6.xml_;_
Call FW_TransactionStart("TC04 TESTWEB -  Click Add to cart")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Add to cart") @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionEnd("TC04 TESTWEB - Click Add to cart")

Call FW_TransactionStart("TC04 TESTWEB - Click View cart")
Call FW_Link("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","View cart")
Call FW_TransactionEnd("TC04 TESTWEB - Click  View cart")

Call FW_TransactionStart("TC04 TESTWEB - Click Go to checkout")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Go to checkout") @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionEnd("TC04 TESTWEB - Click Go to checkout")

Call FW_TransactionStart("TC04 TESTWEB - Input Note") @@ script infofile_;_ZIP::ssf2.xml_;_
Call FW_WebEdit("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","notes",note)
Call FW_TransactionEnd("TC04 TESTWEB - Input Note")
 @@ script infofile_;_ZIP::ssf3.xml_;_
Call FW_TransactionStart("TC04 TESTWEB - Click Proceed to payment")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Proceed to payment") @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionEnd("TC04 TESTWEB - Click Proceed to payment") 

Call FW_TransactionStart("TC04 TESTWEB - Click Place order")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Place order") @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionEnd("TC04 TESTWEB - Click Place order") 


Call FW_TransactionStart("TC04 TESTWEB - Check text Thank you for your order")
Call FW_WebCheckText("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","page-title") @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionEnd("TC04 TESTWEB - Check text Thank you for your order") 
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("page-title").Click

Call FW_TransactionStart("TC04 TESTWEB - Click My account for logout")
Call FW_WebElement("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","My account")
Call FW_TransactionEnd("TC04 TESTWEB - Click My account for logout") 
 @@ script infofile_;_ZIP::ssf20.xml_;_

Call FW_TransactionStart("TC04 TESTWEB - Click Log out")
Call FW_Link("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Log out")
Call FW_TransactionEnd("TC04 TESTWEB - Click Log out")

Call FW_TransactionStart("TC04 TESTWEB - Close Web DEMOSTORE.x-CART")
Call FW_CloseWebBrowser("CHROME")
Call FW_TransactionEnd("TC04 TESTWEB - Close Web DEMOSTORE.x-CART")



'Browser("X-Cart Demo store company").Page("X-Cart Demo store company_2").WebElement("Hot deals").Click @@ script infofile_;_ZIP::ssf7.xml_;_

'Browser("X-Cart Demo store company").Page("X-Cart Demo store company_2").Link("Bestsellers").Click @@ script infofile_;_ZIP::ssf8.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company").Link("Fashion").Click @@ script infofile_;_ZIP::ssf9.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_2").Link("Shoes").Click @@ script infofile_;_ZIP::ssf10.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_3").Image("White Lace-Up Mesh Trainers").Click @@ script infofile_;_ZIP::ssf11.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_4").WebButton("Add to cart").Click @@ script infofile_;_ZIP::ssf12.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_4").Link("View cart").Click @@ script infofile_;_ZIP::ssf13.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebButton("Go to checkout").Click @@ script infofile_;_ZIP::ssf14.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebEdit("notes").Set "test" @@ script infofile_;_ZIP::ssf15.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebButton("Proceed to payment").Click @@ script infofile_;_ZIP::ssf16.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebButton("Place order").Click @@ script infofile_;_ZIP::ssf17.xml_;_

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebElement("page-title").Click @@ script infofile_;_ZIP::ssf18.xml_;_



'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company").Link("Fashion").Click @@ script infofile_;_ZIP::ssf21.xml_;_



'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Fashion").Click @@ script infofile_;_ZIP::ssf22.xml_;_
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Shoes").Click @@ script infofile_;_ZIP::ssf23.xml_;_
