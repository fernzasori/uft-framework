﻿SystemUtil.Run "C:\Program Files (x86)\Micro Focus\Unified Functional Testing\samples\Flights Application\FlightsGUI.exe"

iRowCount = Datatable.getSheet("TC03 [TC03_TestApp]").getRowCount
username = Trim((DataTable("Username","TC03 [TC03_TestApp]")))

password = Trim((DataTable("Password","TC03 [TC03_TestApp]")))


Call FW_TransactionStart("TC03 Input Username")
Call FW_AppFlightEdit("TC03 TEST APP FLIGHT","Micro Focus MyFlight Sample","agentName",username)
Call FW_TransactionEnd("TC03 Input Username")

Call FW_TransactionStart("TC03 Input Password")
Call FW_AppFlightEdit("TC03 TEST APP FLIGHT","Micro Focus MyFlight Sample","password",password)
Call FW_TransactionStart("TC03 Input Password")

Call FW_TransactionStart("TC03 Submit Login Fight")
Call AppFlightButton_Click("TC03 TEST APP FLIGHT","Micro Focus MyFlight Sample","OK")
Call FW_TransactionStart("TC03 Submit Login Fight")

Call FW_TransactionStart("TC03 Find Fight")
Call AppFlightButton_Click("TC03 TEST APP FLIGHT","Micro Focus MyFlight Sample","FIND FLIGHTS")
Call FW_TransactionStart("TC03 Find Fight")

